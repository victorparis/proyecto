# PROYECTO FINAL
# Estadísticas de Aeropuertos españoles.


#### Desarrollo de Aplicaciones Web.

### Víctor París Sarrato, 2º curso.


[Repositorio del proyecto](https://victorparis@bitbucket.org/victorparis/proyecto.git "Proyecto final")

<div class="pagebreak"></div>


### Índice.

1. Contrato del proyecto
	- Lista de requisitos. ------------------------------------------------- *(pág. 3)*
	- Presupuesto. -------------------------------------------------------- *(pág. 4)*
	- Diagrama de tareas.------------------------------------------------ *(pág. 5)*
	- Planificación temporal.--------------------------------------------- *(pág. 6)*
	- Pliego de condiciones.--------------------------------------------- *(pág. 7)*
		- Definición y objeto del contrato.---------------------------- *(pág. 7)*
		- Naturaleza y legislación aplicable.------------------------- *(pág. 7)*
		- Formalización del contrato.---------------------------------- *(pág. 7)*
		- Ejecución del contrato.---------------------------------------- *(pág. 7, 8 y 9)*
2. Documento de análisis y diseño.------------------------------------- *(pág. 10)*
3. Documento de configuración y desarrollo software.------------- *(pág. 11)*
4. Documentación de usuario.-------------------------------------------- *(pág. 12)*
5. Objetivos del proyecto (en inglés).----------------------------------- *(pág. 13)*
6. Diario de bitácora.-------------------------------------------------------- *(pág. 14)*
7. Bibliografía.---------------------------------------------------------------- *(pág. 15)*

<div class="pagebreak"></div>

### Contrato del Proyecto.


#### Lista de requisitos.



- Cada usuario ha de ser único y entrar con su propio nombre de usuario y contraseña.
- La confirmación de registro debería llegar al nuevo usuario por email.
- Una vez realizada dicha confirmación, el usuario ya ha de ser capaz de entrar en la aplicación.
- El usuario debe poder consultar las estadísticas de cada aeropuerto español individualmente y comparar la evolución de las mismas en los últimos 3 años, pudiendo desmarcarse una o varias, según convenga al usuario.
- Los diagramas generados pueden ser imprimidos en el acto o incluso exportados a varios formatos, con tan sólo un botón.
- El usuario también puede consultar tablas comparativas de los aeropuertos más transitados durante los últimos 3 años.


----------

<div class="pagebreak"></div>


#### Presupuesto.

Al no ser necesario el suministro de materiales, el presupuesto únicamente se reduce a la mano de obra, ya que se cuenta con los programas imprescindibles para el correcto desarrollo del proyecto.

Por lo tanto, suponiendo que disponemos de un tiempo máximo para realizar el proyecto de 40 horas y, calculando el valor del mercado actual para la elaboración de este tipo de aplicaciones en 40 €/h. El montante total de la operación quedaría en: 

**40 horas x 40€/h = 1600 €**

----------

<div class="pagebreak"></div>


#### Diagrama de tareas.

![picture alt](diagrama_tareas.jpg "Diagrama secuencial") 


----------

<div class="pagebreak"></div>



#### Planificación temporal.

- Toda la parte de programación y código:

	*Del 24 de Marzo al 30 de Abril.*

- La parte de diseño web:

	*Del 1 de Mayo al 31 de Mayo.*

- Documentación y mejoras del proyecto:

	*Del 1 de Junio hasta fecha de entrega.*

----------

<div class="pagebreak"></div>


#### Pliego de condiciones.


1. **DEFINICIÓN Y OBJETO DEL CONTRATO**


	El contrato tendrá como objeto el suministro a la Empresa XXX de una aplicación de consulta de estadísticas de aeropuertos españoles, que deberá tener las características que se incluyen el pliego de prescripciones técnicas.



2. **NATURALEZA Y LEGISLACIÓN APLICABLE**


	El presente contrato es de carácter administrativo y se regirá por lo establecido en este Pliego, en el que se incluye los pactos y condiciones definidoras de los derechos y obligaciones que asumirán el órgano de contratación, ofertantes y adjudicatarios. 

	Igualmente se regirá por el Pliego de Prescripciones Técnicas donde se recogerán las principales características  técnicas que ha de reunir el objeto de contrato.

	En caso de discrepancia entre el Pliego de Cláusulas Administrativas Particulares y el Pliego de Prescripciones Técnicas, y cualquier otro documento contractual, prevalecerá lo previsto en este Pliego.



3. **FORMALIZACIÓN DEL CONTRATO**

	
	El contrato se formalizará en documento administrativo dentro del plazo de treinta días a contar desde el siguiente al de la notificación de la adjudicación, constituyendo dicho documento título suficiente para acceder a cualquier Registro Público.
	
	Cuando el adjudicatario solicite la formalización del contrato en escritura pública, los gastos  derivados de su otorgamiento serán por cuenta del mismo, debiendo éste aportar a la Administración copia simple de dicha escritura pública.
	
	Simultáneamente con la firma del contrato, deberá ser firmado por el adjudicatario el Pliego de Cláusulas Administrativas Particulares y el de Prescripciones Técnicas.



4. **EJECUCIÓN DEL CONTRATO**


	- PLAZO DE EJECUCIÓN.

		El plazo de ejecución del contrato será el establecido por el contratista y el contratado, contados a partir del día siguiente al de la fecha de la notificación de la adjudicación del contrato.


	- OBLIGACIONES GENERAL DEL CONTRATISTA.


		El desconocimiento del contrato en cualquiera de sus términos, de los documentos anejos que forman parte integrantes del mismo, de las instrucciones y normas de toda índole promulgadas por la Administración, que puedan tener aplicación en la ejecución de lo pactado, no eximirá al contratista de la obligación de su cumplimiento.

		En general, el contratista responderá de cuantas obligaciones le vienen impuestas en su carácter de empleador, así como del cumplimiento de cuantas normas regulan y desarrollan la relación laboral o de otro tipo, existente entre aquél o entre sus subcontratistas y los trabajadores de uno y otro, sin que pueda repercutir contra la Administración ninguna multa, sanción o cualquier tipo de responsabilidad que por incumplimiento de alguna de ellas pudieran imponerle los Organismos competente.

		En cualquier caso, el contratista indemnizará a la Administración de toda cantidad que se viese obligada a pagar por incumplimiento de las obligaciones aquí consignadas, aunque ello le venga impuesto por resolución judicial o administrativa.


	- CUMPLIMIENTO DEL CONTRATO.


		El contrato de suministro se entenderá cumplido por el contratista cuando éste haya realizado, de acuerdo con los términos del mismo y la satisfacción de la Administración, la totalidad de su objeto.


	- ABONO.

		
		El contratista tiene derecho al abono del precio convenido en el contrato.
		
		El pago podrá hacerse de una sola vez o fraccionarse, en función de los plazos parciales que se hubieren establecidos.
		
		El pago del precio se efectuará previa presentación de factura por triplicado ejemplar, debiendo ser repercutido como partida independiente el Impuesto sobre el Valor Añadido en el documento que se presente para el contrato, sin que el importe global contratado experimente incremento alguno.


	- PLAZO DE GARANTÍA.

		
		El plazo de garantía será de 90 días y comenzará a contarse a partir del día siguiente al de la recepción del suministro.

		Durante el plazo de garantía el contratista tiene derecho a ser oído y a vigilar la aplicación.

		Terminado el plazo de garantía sin que la Administración haya formalizado alguno de los reparos o la denuncia a que se refieren los anteriores apartados, el contratista quedará exento de responsabilidad.


	- RESOLUCIÓN DEL CONTRATO. PENALIDADES.


		El contratista queda obligado al cumplimiento del plazo de ejecución del contrato. 
		
		Si llegado el término del plazo final, el contratista hubiera incurrido en demora por causas imputables al mismo, la Administración podrá optar indistintamente, en las formas y condiciones establecidas en el artículo 96 del TR de la LCAP,  por la resolución del contrato con pérdida de la garantía definitiva o por imposición de las penalidades establecidas en el mismo.
		
		El importe de las penalidades no excluye la indemnización a que pueda tener derecho la Administración por daños y perjuicios originados por la demora del contratista.
		
		Si el retraso fuera por motivos no imputables al contratista, se estará a lo dispuesto en el artículo 97.2 del TR de la LCAP.


	- RECURSOS Y JURISDICCIÓN.


		Las cuestiones litigiosas surgidas sobre la interpretación, modificación, resolución y efectos de los contratos administrativos, serán resueltas por el órgano de contratación competente, cuyos acuerdos pondrán fin a la vía administrativa, y contra los mismos se podrá interponer potestativamente el recurso de reposición o el oportuno recurso contencioso administrativo.


----------



<div class="pagebreak"></div>


### Documento de análisis y diseño.

 1. Análisis
 	
	Una vez realizadas entrevistas, encuestas y cuestionarios a usuarios finales, así como también, consultas a documentos y manuales que contienen normas de procedimientos de operación, los datos ha sido analizados para establecer cómo es el flujo de información y dectectar la posible causa de que este flujo sea defectuoso.

	Los resultados han sido altamente positivos y se espera una gran acogida de la aplicación.



 2. Diseño.
	
	La aplicación es factible económicamente. Está comprobado y verificado que los costos son justificados por los beneficios que va a ofrecer.

	El hardware, software y personal necesarios para llevar a cabo el proyecto son escasos, lo cual hace a la aplicación técnicamente factible.

	También posee una factibilidad operativa elevada, ya que puede ser aceptada por usuarios de cualquier nivel.

	Se adapta al sistema general de información de cualquier organización.

	Posee gran flexibilidad para aceptar modificaciones futuras.

	Ofrece seguridad contra el uso no autorizado.


<div class="pagebreak"></div>



### Documento de configuración y desarrollo software.

Lo primero de todo, hay que tener instalado phpmyadmin y php. Si no los tienes, puedes descargar los paquetes de [aquí](http://www.appservnetwork.com/).


Para un correcto funcionamiento del proyecto, debemos crear la base de datos, sus tablas e insertar los datos correspondientes. Sigue estas instrucciones, una vez tengas localizada la ubicación del proyecto:

1. Dentro de la carpeta **"bd"**, ejecuta el archivo **"01-Proyecto-CREATE-BD.php"** en tu navegador.
2. En la misma carpeta, haz lo mismo con el archivo **"02-Airports-CREATE-TABLE.php"**.
3. Ahora ejecuta **"03-Airports-INSERT.php"** y selecciona los archivos .csv a importar. Estos son: ***estadisticas2011.csv, estadisticas2012.csv, estadisticas2013.csv.***
4. Finalmente, ejecuta el archvo **"04-Users-CREATE-TABLE.php"**.

Una vez realizados estos 4 pasos, ya podrás ingresar en la aplicación registrándote y podrás visualizar las estadísticas de los aeropuertos españoles.

*Recuerda que como se trabaja bajo servidor local y se interactúa con phpMyAdmin, deberás modificar el nombre de usuario y contraseña de los archivos anteriores para un correcto funcionamiento.*



<div class="pagebreak"></div>


### Documentación de usuario.


Esta aplicación de visualización de estadísticas de aeropuertos españoles ha sido diseñada para una fácil navegación del usuario.

Lo primero de todo, si no posees una cuenta, deberás registrarte.

Una vez verificados y almacenados tus datos, ya puedes ingresar en la aplicación.

El menú superior muestra tu nombre de usuario y avatar, en este caso predeterminado. Si pulsas sobre tu nombre, podrás cerrar la sesión una vez hayas terminado de realizar consultas.

La barra de menú lateral izquierda muestra las estadísticas a las que puedes acceder, tanto de cada aeropuerto (ordenados por comunidad autónoma) como de un conjunto de aeropuertos más transitados.

Una vez seleccionado un aeropuerto, la página central mostrará una tabla con sus correspondientes datos. Pincha en la leyenda a la derecha si deseas ocultar o mostrar información en concreto. También podrás descargarte en varios formatos las tablas a tu ordenador o imprimirlas directamente. Tan sólo pulsa el botón de la tabla situado en la esquina superior derecha y elige una opción.

Si deseas ponerte en contacto con el administrador para hacerle alguna pregunta o sugerencia, puedes hacerlo tanto en la parte inferior del navegador como en la pestaña de contancto situada también en la barra de menú lateral izquierda.



<div class="pagebreak"></div>


### Objetivos del proyecto (en inglés).

I have been working in a company that works with open data and import them to its own application. This application shows information about hostels around Europe: Number of hostels in a city, nights spent, bed places, gross and net occupancy rate of beds or bedrooms...

So, I have wanted to take that idea and do something similar but with all the airports in Spain.

Actually, it's not an easy work to find open data about flights or passengers but I finally found them in the Aena's webpage and I could download them as .csv files.

I have created a PHP script that interacts with a MySQL's database and I have uploaded the files there.

Anyway, as a user, first of all you must register to join the application. Then, you will be able to look for information about any spanish airport. The app will display diagrams, statics or charts from 2011 to 2013.

As you can see, there are too many airports; exactly 52. Sites like Germany has just 39 with twice the population.
This is Spain.


<div class="pagebreak"></div>


### Diario de bitácora.

- Pensar y organizar la idea y boceto del proyecto (2 horas).
- Recopilar datos de aeropuertos de distintas páginas (2 horas).
- Crear la base de datos, las tablas de los aeropuertos y la inserción de datos mediante archivos .csv con php (5 horas).
- Crear código para visualizar los datos de la base de datos mediante diagramas (15 horas).
- Capturas de pantalla en google maps de todas las ciudades españolas con aeropuerto y recorte de imágenes (2 horas).
- Páginas de login, registro y logout en PHP (2 horas).
- Diseño bootstrap de la aplicación (15 horas).
- Slider de la página de inicio (2 horas).
- Resto de diseño de la aplicación (5 horas).
 


<div class="pagebreak"></div>


### Bibliografía.

- [AENA](http://www.aena.es/csee/Satellite/Home "AENA")
- [Highcharts](http://www.highcharts.com/ "Highcharts")
- [Bootstrap](http://getbootstrap.com/ "Bootstrap")
- Ayudas [PHP](http://www.w3schools.com/PHP/ "PHP")
- [Mapas](https://www.google.es/maps/preview "Mapas")
- Apuntes de clase de [Luismi](http://moodle.cpilosenlaces.com/course/view.php?id=266) y [Juanda](http://www.formandome.es/)