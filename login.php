<?php
	session_start();
	
	$username = $_SESSION['username'];
	$password = $_SESSION['password'];
         
		if($username && $password) {
			header( 'Location: index.php' ) ;
		} else {
        	function index() {
               
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Estadisticas Aeropuertos</title>
	<meta name="description" content="description">
	<meta name="author" content="Victor Paris">
	<meta name="keyword" content="keywords">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="css/bootstrap.css" rel="stylesheet">
	<!-- <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet"> -->
	<link href='http://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>
	<link href="css/style.css" rel="stylesheet">
</head>

<body>
	<div class="container-fluid">
		<div id="page-login" class="row">
			
			<!-- offset es un margen izquiero, lo cual ayuda a centrar la caja -->
			<div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 ">
				<div class="text-right">
					<a href="register.php" class="log-reg">¿Necesitas crearte una cuenta?</a>
				</div>
				<form action='' method='post'>
					<div class="box">
						<div class="box-form">
							<div class="text-center">
								<h3 class="log-reg-header">Estadísticas de Aeropuertos - ¡Únete!</h3>
							</div>
							<div>
								<label>Usuario</label>
								<input type="text" class="form-control" name="username" />
							</div>
							<div>
								<label class="control-label">Contraseña</label>
								<input type="password" class="form-control" name="password" />
							</div><br>
							<div class="text-center">
								<input type='submit' class="button" value='Unirse' name='login'/>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>
      
<?php		 
 
	}
    
    function login() {
			
		$username = $_REQUEST['username'];
       	$password = $_REQUEST['password'];

        if ($username=="") {
			die("<br /> You Forgot to type in the Username for the user !<br /> ");
			//header( 'Location: login.php' );
		}
	   
	   	if ($password=="") {
	      	die("<br />You Forgot to type in your password! ");
	      	//header( 'Location: login.php' );
	   	}
	
	   	$connect = mysql_connect("localhost", "root", "alumno");
	   	
	   	if(!$connect) {
	    	die(mysql_error());
	    	//header( 'Location: login.php' );
	   	}
	   	
	   	$select_db = mysql_select_db("Proyecto");
	   
	   	if(!$select_db) {
	    	die(mysql_error());
			//header( 'Location: login.php' );
	   	}
	
	   	$result = mysql_query("SELECT * FROM users WHERE username='$username' ");
	   	$row = mysql_fetch_array($result);
		$user = $row['username'];
	   	
	   	if($username != $user) {
	      	die("<br />Username is wrong!<br /> ");
	      	//header( 'Location: login.php' );
	   	}
	
	   	$real_password = $row['password'];
	   	if($password != $real_password) {
	    	die("<br />Your password is wrong!<br /> ");
	    	//header( 'Location: login.php' );
	   	}
	
	   	$_SESSION['username']=$username;
	   	$_SESSION['password']=$password;
	
	   	header( 'Location: index.php' ) ;
	}
	
	if (isset($_REQUEST['login'])) { 
		login(); 
	} else {
		index();
	}
}

?>