
#Proyecto final

### Estadísticas de Aeropuertos

#### por Víctor París.


- Instalación y configuración.

Lo primero de todo, hay que tener instalado phpmyadmin y php. Si no los tienes, puedes descargar los paquetes de [aquí](http://www.appservnetwork.com/).

Para un correcto funcionamiento del proyecto, debemos crear la base de datos, sus tablas e insertar los datos correspondientes. Sigue estas instrucciones, una vez tengas localizada la ubicación del proyecto:

1. Dentro de la carpeta "*bd*", ejecuta el archivo "**01_Proyecto_CREATE_BD.php**" en tu navegador. 

2. En la misma carpeta, haz lo mismo con el archivo "**02_Airports_CREATE_TABLE.php**".

3. Ahora ejecuta "**03_Airports_INSERT.php**" y selecciona los archivos **.csv** a importar. Estos son: ** *estadisticas2011.csv, estadisticas2012.csv, estadisticas2013.csv* **

4. Finalmente, ejecuta el archvo "**04_Users_CREATE_TABLE.php**".


Una vez realizados estos 4 pasos, ya podrás ingresar en la aplicación registrándote y podrás visualizar las estadísticas de los aeropuertos españoles.

*Recuerda que como se trabaja bajo servidor local y se interactúa con phpMyAdmin, deberás modificar el nombre de usuario y contraseña de los archivos anteriores para un correcto funcionamiento*.