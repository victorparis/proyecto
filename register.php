
<?php 
	session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Estadisticas Aeropuertos</title>
	<meta name="description" content="description">
	<meta name="author" content="Victor Paris">
	<meta name="keyword" content="keywords">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>
	<link href="css/style.css" rel="stylesheet">
</head>

<body>
	<div class="container-fluid">
		<div id="page-login" class="row">
			<div class="col-xs-12 col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
				<div class="text-right">
					<a href="login.php" class="log-reg">¿Ya tienes una cuenta?</a>
				</div>
				<form action='' method='post'>
					<div class="box">
						<div class="box-form">
							<div class="text-center">
								<h3 class="log-reg-header">Estadísticas de Aeropuertos - ¡Regístrate!</h3>
							</div>
							<div>
								<label class="control-label">Usuario</label>
								<input type="text" class="form-control" name="user" />
							</div>
							<div>
								<label class="control-label">Correo</label>
								<input type="text" class="form-control" name="email" />
							</div>
							<div>
								<label class="control-label">Contraseña</label>
								<input type="password" class="form-control" name="pass" />
							</div><br>
							<div class="text-center">
								<input type='submit' class="button" value='Registrarse' name='Register'/>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>

    
<?php

	if (isset($_REQUEST['Register'])) {
		$username = $_REQUEST['user'];
	 	$pass = $_REQUEST['pass'];
	
	 	if ($username=="") {
	    	//die("<br /> You Forgot to type in the Username for the user !<br /> ");
	    	header('Location: register.php');
	 	}
	 
	 	if ($pass=="") {
	    	//die("<br /> You Forgot to type in the Password for the user !<br /> ");
			header('Location: register.php');
	 	}
	
	 	$connect = mysql_connect("localhost", "root", "alumno");
	 	if(!$connect) {
	    	//die(mysql_error());
			header('Location: register.php');
	 	}
	
	 	$select_db = mysql_select_db("Proyecto");
	 	if(!$select_db) {
	     	//die(mysql_error());
			header('Location: register.php');
	 	}
	
	 	$result = mysql_query("INSERT INTO users (username, password) VALUES ('$username', '$pass') ");
	
	 	if($result){
			header('Location: index.php');
	 	}
	}
	
?>

