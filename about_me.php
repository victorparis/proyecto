<?php 
    session_start();
    $username = $_SESSION['username'];
    $password = $_SESSION['password'];
    if(!$username && !$password)
    {
       header( 'Location: login.php' ) ;
    }
    else
   {
   	
?>
		   
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Estadisticas Aeropuertos</title>
	<meta name="description" content="description">
	<meta name="author" content="Victor Paris">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="css/bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" href="css/slide.css" type="text/css" media="screen">
	<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>
	<link href="css/style.css" rel="stylesheet">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/slide.js"></script>
	<script type="text/javascript">
	$(function () {
    var chart;
    $(document).ready(function() {
    	
    	//A CORUÑA **********************
	    $('.A_Coruna').click(function(){
	    	$.getJSON("data/data_A_CORUNA.php", function(json) {
			    chart = new Highcharts.Chart({
			    	//tipo de diagrama (en este caso lineal) y dónde lo va a mostrar (en el div container)
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros A Coruña',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            // nos muestra el texto que queremos que salga en la ventana al pasar el ratón sobre un dato
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
		        // le agrego una imagen de fondo que ocupe el 100% de la tabla
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.3).add();
    				chart.renderer.image('imagenes/a_coruna.jpg', 'auto', 'auto', '100%', '100%').add(group); 
				});
	        });
        });
        
        //ALBACETE **********************
	    $('.Albacete').click(function(){
	    	$.getJSON("data/data_ALBACETE.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Albacete',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/albacete.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //ALGECIRAS **********************
	    $('.Algeciras').click(function(){
	    	$.getJSON("data/data_ALGECIRAS.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Algeciras',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.2).add();
    				chart.renderer.image('imagenes/algeciras.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //ALICANTE **********************
	    $('.Alicante').click(function(){
	    	$.getJSON("data/data_ALICANTE.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Alicante',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/alicante.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //ALMERIA **********************
	    $('.Almeria').click(function(){
	    	$.getJSON("data/data_ALMERIA.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Almería',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/almeria.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //ASTURIAS **********************
	    $('.Asturias').click(function(){
	    	$.getJSON("data/data_ASTURIAS.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Asturias',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/aviles.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //BADAJOZ **********************
	    $('.Badajoz').click(function(){
	    	$.getJSON("data/data_BADAJOZ.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Badajoz',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/badajoz.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //BARCELONA **********************
	    $('.Barcelona').click(function(){
	    	$.getJSON("data/data_BARCELONA.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Barcelona',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/barcelona.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //BILBAO **********************
	    $('.Bilbao').click(function(){
	    	$.getJSON("data/data_BILBAO.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Bilbao',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.2).add();
    				chart.renderer.image('imagenes/bilbao.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
    	//BURGOS **********************
	    $('.Burgos').click(function(){
	    	$.getJSON("data/data_BURGOS.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Burgos',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/burgos.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
	        
        // CEUTA
        $('.Ceuta').click(function(){
        	$.getJSON("data/data_CEUTA.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Ceuta',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/ceuta.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
	    });
	    
	    //CORDOBA **********************
	    $('.Cordoba').click(function(){
	    	$.getJSON("data/data_CORDOBA.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Córdoba',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/cordoba.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //EL HIERRO **********************
	    $('.El_Hierro').click(function(){
	    	$.getJSON("data/data_EL_HIERRO.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros El Hierro',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/el_hierro.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //FUERTEVENTURA **********************
	    $('.Fuerteventura').click(function(){
	    	$.getJSON("data/data_FUERTEVENTURA.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Fuerteventura',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/fuerteventura.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //GIRONA **********************
	    $('.Girona').click(function(){
	    	$.getJSON("data/data_GIRONA.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Girona',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/girona.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //GRAN CANARIA **********************
	    $('.Gran_Canaria').click(function(){
	    	$.getJSON("data/data_GRAN_CANARIA.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Gran Canaria',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/gran_canaria.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //GRANADA **********************
	    $('.Granada').click(function(){
	    	$.getJSON("data/data_GRANADA.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Granada',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/granada.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //HUESCA **********************
	    $('.Huesca').click(function(){
	    	$.getJSON("data/data_HUESCA.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Huesca',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/huesca.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //IBIZA **********************
	    $('.Ibiza').click(function(){
	    	$.getJSON("data/data_IBIZA.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Ibiza',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/ibiza.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //JEREZ DE LA FRONTERA **********************
	    $('.Jerez').click(function(){
	    	$.getJSON("data/data_JEREZ.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Jerez de la Frontera',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/jerez.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //LA GOMERA **********************
	    $('.La_Gomera').click(function(){
	    	$.getJSON("data/data_LA_GOMERA.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros La Gomera',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/la_gomera.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //LA PALMA **********************
	    $('.La_Palma').click(function(){
	    	$.getJSON("data/data_LA_PALMA.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros La Palma',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/la_palma.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //LANZAROTE **********************
	    $('.Lanzarote').click(function(){
	    	$.getJSON("data/data_LANZAROTE.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Lanzarote',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/la_palma.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //LEON **********************
	    $('.Leon').click(function(){
	    	$.getJSON("data/data_LEON.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros León',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/leon.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //LOGROÑO **********************
	    $('.Logrono').click(function(){
	    	$.getJSON("data/data_LOGRONO.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Logroño',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/logrono.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //MADRID **********************
	    $('.Madrid').click(function(){
	    	$.getJSON("data/data_MADRID.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Madrid',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/madrid.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //MADRID-CUATRO VIENTOS **********************
	    $('.Madrid_CV').click(function(){
	    	$.getJSON("data/data_MADRID_CV.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Madrid-Cuatro Vientos',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/madrid_c.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //MADRID-TORREJON **********************
	    $('.Madrid_T').click(function(){
	    	$.getJSON("data/data_MADRID_T.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Madrid-Torrejón',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/madrid_t.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //MALAGA **********************
	    $('.Malaga').click(function(){
	    	$.getJSON("data/data_MALAGA.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Málaga',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/malaga.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //MELILLA **********************
	    $('.Melilla').click(function(){
	    	$.getJSON("data/data_MELILLA.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Melilla',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/melilla.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //MENORCA **********************
	    $('.Menorca').click(function(){
	    	$.getJSON("data/data_MENORCA.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Menorca',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/menorca.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //MURCIA **********************
	    $('.Murcia').click(function(){
	    	$.getJSON("data/data_MURCIA.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Murcia',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/murcia.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //PALMA DE MALLORCA **********************
	    $('.Palma').click(function(){
	    	$.getJSON("data/data_PALMA.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Palma de Mallorca',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/mallorca.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //PAMPLONA **********************
	    $('.Pamplona').click(function(){
	    	$.getJSON("data/data_PAMPLONA.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Pamplona',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/pamplona.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //REUS **********************
	    $('.Reus').click(function(){
	    	$.getJSON("data/data_REUS.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Reus',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/reus.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //SABADELL **********************
	    $('.Sabadell').click(function(){
	    	$.getJSON("data/data_SABADELL.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Sabadell',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/sabadell.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //SALAMANCA **********************
	    $('.Salamanca').click(function(){
	    	$.getJSON("data/data_SALAMANCA.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Salamanca',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/salamanca.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //SAN SEBASTIAN **********************
	    $('.San_Sebastian').click(function(){
	    	$.getJSON("data/data_SAN_SEBASTIAN.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros San Sebastián',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/san_sebastian.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //SANTANDER **********************
	    $('.Santander').click(function(){
	    	$.getJSON("data/data_SANTANDER.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Santander',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/santander.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //SANTIAGO **********************
	    $('.Santiago').click(function(){
	    	$.getJSON("data/data_SANTIAGO.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Santiago',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/santiago.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //SEVILLA **********************
	    $('.Sevilla').click(function(){
	    	$.getJSON("data/data_SEVILLA.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Sevilla',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/sevilla.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //SON BONET **********************
	    $('.Son_Bonet').click(function(){
	    	$.getJSON("data/data_SON_BONET.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Son Bonet',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/son_bonet.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //TENERIFE NORTE **********************
	    $('.Tenerife_N').click(function(){
	    	$.getJSON("data/data_TENERIFE_N.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Tenerife Norte',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/tenerife_n.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //TENERIFE SUR **********************
	    $('.Tenerife_S').click(function(){
	    	$.getJSON("data/data_TENERIFE_S.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Tenerife Sur',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/tenerife_s.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //VALENCIA **********************
	    $('.Valencia').click(function(){
	    	$.getJSON("data/data_VALENCIA.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Valencia',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/valencia.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //VALLADOLID **********************
	    $('.Valladolid').click(function(){
	    	$.getJSON("data/data_VALLADOLID.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Valladolid',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/valladolid.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //VIGO **********************
	    $('.Vigo').click(function(){
	    	$.getJSON("data/data_VIGO.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Vigo',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/vigo.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //VITORIA **********************
	    $('.Vitoria').click(function(){
	    	$.getJSON("data/data_VITORIA.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Vitoria',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/vitoria.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //ZARAGOZA **********************
	    $('.Zaragoza').click(function(){
	    	$.getJSON("data/data_ZARAGOZA.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'line',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Pasajeros Zaragoza',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/zaragoza.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //ESTADÍSTICAS 2011 **********************
	    $('.2011').click(function(){
	    	$.getJSON("data/data_2011.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'column',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Estadísticas 2011',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/espana.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //ESTADÍSTICAS 2012 **********************
	    $('.2012').click(function(){
	    	$.getJSON("data/data_2012.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'column',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Estadísticas 2012',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/espana.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
        //ESTADÍSTICAS 2013 **********************
	    $('.2013').click(function(){
	    	$.getJSON("data/data_2013.php", function(json) {
			    chart = new Highcharts.Chart({
		            chart: {
		                renderTo: 'container',
		                type: 'column',
		                marginRight: 130,
		                marginBottom: 25
		            },
		            title: {
		                text: 'Estadísticas 2013',
		                x: -20 //center
		            },
		            subtitle: {
		                text: '',
		                x: -20
		            },
		            xAxis: {
		                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']		                
		            },
		            yAxis: {
		                title: {
		                    text: 'Pasajeros'
		                },
		                plotLines: [{
		                    value: 0,
		                    width: 1,
		                    color: '#808080'
		                }]
		            },
		            tooltip: {
		                formatter: function() {
		                        return '<b>'+ this.series.name +'</b><br/>'+
		                        this.x +': '+ this.y;
		                }
		            },
		            legend: {
		                layout: 'vertical',
		                align: 'right',
		                verticalAlign: 'top',
		                x: -10,
		                y: 100,
		                borderWidth: 0
		            },
		            series: json
		        }, 
	            function(chart) { // on complete
     				var group = chart.renderer.g().attr('opacity',0.4).add();
    				chart.renderer.image('imagenes/espana.jpg', 'auto', 'auto', '100%', '100%').add(group); 
		        });
	        });
        });
        
    });
});	

	</script>
		
</head>
<body>
	<script src="json/highcharts.js"></script>
  	<script src="json/exporting.js"></script>
	
<!--Start Header-->

<header class="navbar">
	<div class="container-fluid expanded-panel">
		<div class="row">
			<div id="logo" class="col-xs-12 col-sm-2">
				<a href="#">AEROconsult</a>
			</div>
			<div id="top-panel" class="col-xs-12 col-sm-10">
				<div class="row">
					<div class="col-xs-8 col-sm-4">
						<a href="#" class="show-sidebar">
						  <i class="fa fa-bars"></i>
						</a>
						<div id="search">
							<input type="text" placeholder="search"/>
							<i class="fa fa-search"></i>
						</div>
					</div>
					<div class="col-xs-4 col-sm-8 top-panel-right">
						<ul class="nav navbar-nav pull-right panel-menu">
							<li class="hidden-xs hidden-sm">
								<a href="#">
									<i class="fa fa-bell"></i>
									<span class="badge">2</span>
								</a>
							</li>
							<li class="hidden-xs hidden-sm">
								<a href="#">
									<i class="fa fa-calendar"></i>
									<span class="badge">1</span>
								</a>
							</li>
							<li class="hidden-xs hidden-sm">
								<a href="#">
									<i class="fa fa-envelope"></i>
									<span class="badge">4</span>
								</a>
							</li>							
							
							<li class="dropdown">
								<a href="#" class="dropdown-toggle account" data-toggle="dropdown">
									<div class="avatar">
										<img src="imagenes/avion.gif" class="img-rounded" alt="avatar" /> 
									</div>
									<i class="fa fa-angle-down pull-right"></i>
									<div class="user-mini pull-right">
										<span class="welcome">Welcome,</span>
										<span>
										<?php
											echo "$username";
										?>
										</span>
									</div>
								</a>
								
								<ul class="dropdown-menu">
									
									<li>
										<a href="logout.php">
											<i class="fa fa-power-off"></i>
											<span class="hidden-sm text">Logout</span>
										</a>
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
<!--End Header-->

<!--Start Container-->
<div id="main" class="container-fluid">
	<div class="row">
		<div id="sidebar-left" class="col-xs-2 col-sm-2">
			<ul class="nav main-menu">
				<li class="dropdown">
					<a href="index.php" class="dropdown-toggle">
						<i class="fa fa-home fa-fw"></i>
						<span class="hidden-xs">Inicio</span>
					</a>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle">
						<i class="fa fa-book fa-fw"></i>
						<span class="hidden-xs">Historia</span>
					</a>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle">
						<i class="fa fa-plane"></i>
						<span class="hidden-xs">Todos los aeropuertos</span>
					</a>
					<ul class="dropdown-menu">
						
						<li>
						<a href="#" class="dropdown-toggle">
							<span class="hidden-xs">ANDALUCIA</span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a class="Algeciras" href="#">Algeciras</a>
							</li>
							<li>
								<a class="Almeria" href="#">Almería</a>
							</li>
							<li>
								<a class="Cordoba" href="#">Córdoba</a>
							</li>
							<li>
								<a class="Granada" href="#">Granada</a>
							</li>
							<li>
								<a class="Jerez" href="#">Jerez de la Frontera</a>
							</li>
							<li>
								<a class="Malaga" href="#">Málaga</a>
							</li>
							<li>
								<a class="Sevilla" href="#">Sevilla</a>
							</li>
						</ul>
						</li>
						
						<li>
						<a href="#" class="dropdown-toggle">
							<span class="hidden-xs">ARAGON</span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a class="Huesca" href="#">Huesca</a>
							</li>
							<li>
								<a class="Zaragoza" href="#">Zaragoza</a>
							</li>
						</ul>
						</li>
						
						<li>
						<a href="#" class="dropdown-toggle">
							<span class="hidden-xs">CANTABRIA</span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a class="Santander" href="#">Santander</a>
							</li>
						</ul>
						</li>
						
						<li>
						<a href="#" class="dropdown-toggle">
							<span class="hidden-xs">CASTILLA Y LEON</span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a class="Burgos" href="#">Burgos</a>
							</li>
							<li>
								<a class="Leon" href="#">León</a>
							</li>
							<li>
								<a class="Salamanca" href="#">Salamanca</a>
							</li>
							<li>
								<a class="Valladolid" href="#">Valladolid</a>
							</li>
						</ul>
						</li>
						
						<li>
						<a href="#" class="dropdown-toggle">
							<span class="hidden-xs">CASTILLA-LA MANCHA</span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a class="Albacete" href="#">Albacete</a>
							</li>
						</ul>
						</li>
						
						<li>
						<a href="#" class="dropdown-toggle">
							<span class="hidden-xs">CATALUÑA</span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a class="Barcelona" href="#">Barcelona</a>
							</li>
							<li>
								<a class="Girona" href="#">Girona</a>
							</li>
							<li>
								<a class="Reus" href="#">Reus</a>
							</li>
							<li>
								<a class="Sabadell" href="#">Sabadell</a>
							</li>
						</ul>
						</li>
						
						<li>
						<a href="#" class="dropdown-toggle">
							<span class="hidden-xs">COM. DE MADRID</span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a class="Madrid" href="#">Madrid</a>
							</li>
							<li>
								<a class="Madrid_CV" href="#">Madrid-Cuatro Vientos</a>
							</li>
							<li>
								<a class="Madrid_T" href="#">Madrid-Torrejón</a>
							</li>
						</ul>
						</li>
						
						<li>
						<a href="#" class="dropdown-toggle">
							<span class="hidden-xs">COM. VALENCIANA</span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a class="Alicante" href="#">Alicante</a>
							</li>
							<li>
								<a class="Valencia" href="#">Valencia</a>
							</li>
						</ul>
						</li>
						
						<li>
						<a href="#" class="dropdown-toggle">
							<span class="hidden-xs">EXTREMADURA</span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a class="Badajoz" href="#">Badajoz</a>
							</li>
						</ul>
						</li>
						
						<li>
						<a href="#" class="dropdown-toggle">
							<span class="hidden-xs">GALICIA</span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a class="A_Coruna" href="#">A Coruña</a>
							</li>
							<li>
								<a class="Santiago" href="#">Santiago</a>
							</li>
							<li>
								<a class="Vigo" href="#">Vigo</a>
							</li>
						</ul>
						</li>
						
						<li>
						<a href="#" class="dropdown-toggle">
							<span class="hidden-xs">ISLAS BALEARES</span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a class="Ibiza" href="#">Ibiza</a>
							</li>
							<li>
								<a class="Menorca" href="#">Menorca</a>
							</li>
							<li>
								<a class="Palma" href="#">Palma de Mallorca</a>
							</li>
							<li>
								<a class="Son_Bonet" href="#">Son Bonet</a>
							</li>
						</ul>
						</li>
						
						<li>
						<a href="#" class="dropdown-toggle">
							<span class="hidden-xs">ISLAS CANARIAS</span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a class="El_Hierro" href="#">El Hierro</a>
							</li>
							<li>
								<a class="Fuerteventura" href="#">Fuerteventura</a>
							</li>
							<li>
								<a class="Gran_Canaria" href="#">Gran Canaria</a>
							</li>
							<li>
								<a class="La_Gomera" href="#">La Gomera</a>
							</li>
							<li>
								<a class="La_Palma" href="#">La Palma</a>
							</li>
							<li>
								<a class="Lanzarote" href="#">Lanzarote</a>
							</li>
							<li>
								<a class="Tenerife_N" href="#">Tenerife Norte</a>
							</li>
							<li>
								<a class="Tenerife_S" href="#">Tenerife Sur</a>
							</li>
						</ul>
						</li>
						
						<li>
						<a href="#" class="dropdown-toggle">
							<span class="hidden-xs">LA RIOJA</span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a class="Logrono" href="#">Logroño</a>
							</li>
						</ul>
						</li>
						
						<li>
						<a href="#" class="dropdown-toggle">
							<span class="hidden-xs">NAVARRA</span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a class="Pamplona" href="#">Pamplona</a>
							</li>
						</ul>
						</li>
						
						<li>
						<a href="#" class="dropdown-toggle">
							<span class="hidden-xs">PAIS VASCO</span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a class="Bilbao" href="#">Bilbao</a>
							</li>
							<li>
								<a class="San_Sebastian" href="#">San Sebastian</a>
							</li>
							<li>
								<a class="Vitoria" href="#">Vitoria</a>
							</li>
						</ul>
						</li>
						
						<li>
						<a href="#" class="dropdown-toggle">
							<span class="hidden-xs">PRINC. DE ASTURIAS</span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a class="Asturias" href="#">Aviles</a>
							</li>
						</ul>
						</li>
						
						<li>
						<a href="#" class="dropdown-toggle">
							<span class="hidden-xs">REGION DE MURCIA</span>
						</a>
						<ul class="dropdown-menu">
							<li>
								<a class="Murcia" href="#">Murcia</a>
							</li>
						</ul>
						</li>
						
					</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle">
						<i class="fa fa-bar-chart-o"></i>
						<span class="hidden-xs">Los más transitados</span>
					</a>
					<ul class="dropdown-menu">
						<li>
							<a class="2013" href="#">2013</a>
						</li>
						<li>
							<a class="2012" href="#">2012</a>
						</li>
						<li>
							<a class="2011" href="#">2011</a>
						</li>
						
					</ul>
				</li>
				<li class="dropdown">
					<a href="about_me.php" class="dropdown-toggle">
						<i class="fa fa-cog fa-fw"></i>
						<span class="hidden-xs">Contacto</span>
					</a>
				</li>
			</ul>
		</div>
		
		<!--Start Content-->
		<div id="content" class="col-sm-10">
			<div id="container">
				<div class="aboutus">
			         <div class="row">
			            <div class="col-md-12">
			               <!-- Staff #1 -->
			               <div class="row">
			                  <div class="col-md-6 col-sm-6">
			                     <div class="col-l">
			                       <!-- Staff pic -->
			                       <div class="pic">
			                          <img src="imagenes/victor2.jpg" alt="" class="img-responsive" />
			                       </div>
			                     </div>
			
			                     <div class="col-r">
			                       <div class="sinfo">
			                          <!-- Name -->
			                          <h4>Víctor París</h4>
			                          <!-- Designation -->
			                          <h6 class="grey">Web Developer</h6>
			                          <!-- Social media links. Replace # with profile links -->
			                          <div class="social">
			                             <a href="#" class="tip" data-original-title="Facebook"><i class="fa fa-facebook facebook"></i></a>
			                             <a href="#" class="tip" data-original-title="Twitter"><i class="fa fa-twitter twitter"></i></a>
			                             <a href="#" class="tip" data-original-title="Google Plus"><i class="fa fa-google-plus google-plus"></i></a>
			                             <a href="#" class="tip" data-original-title="LinkedIn"><i class="fa fa-linkedin linkedin"></i></a>
			                          </div>
			                       </div>
			                     </div>
			
			                     <div class="clearfix"></div>
			                  		</div>
			                  <div class="col-md-6 col-sm-6">
			                     <!-- Small para -->
   			                     <p class="ainfo">Soy un desarrollador web y en estos momentos estoy trabajando en varios proyectos. Tengo una amplia experiencia en el campo de la programación y el diseño web. Si estás interesado en contratar mis servicios, no dudes en ponerte en contacto conmigo. Espero que disfrutes navegando por mi web.</p>
			                     <input type='submit' class="button" value='Contacta' name='contacta'/>
			                  </div>
			               </div>
			            </div>
			         </div>
			   </div> 
			   
			   <div class="aboutus">
			         <div class="row">
			            <div class="col-md-12">
			               <!-- Staff #1 -->
			               <div class="row">
			                  <div class="col-md-6 col-sm-6">
			                     <div class="col-l">
			                       <!-- Staff pic -->
			                       <div class="pic">
			                          <img src="imagenes/avatar.gif" alt="" class="img-responsive" />
			                       </div>
			                     </div>
			
			                     <div class="col-r">
			                       <div class="sinfo">
			                          <!-- Name -->
			                          <h4>Personaje Anónimo</h4>
			                          <!-- Designation -->
			                          <h6 class="grey">Puesto de trabajo ¿?</h6>
			                          <!-- Social media links. Replace # with profile links -->
			                          <div class="social">
			                             <a href="#" class="tip" data-original-title="Facebook"><i class="fa fa-facebook facebook"></i></a>
			                             <a href="#" class="tip" data-original-title="Twitter"><i class="fa fa-twitter twitter"></i></a>
			                             <a href="#" class="tip" data-original-title="Google Plus"><i class="fa fa-google-plus google-plus"></i></a>
			                             <a href="#" class="tip" data-original-title="LinkedIn"><i class="fa fa-linkedin linkedin"></i></a>
			                          </div>
			                       </div>
			                     </div>
			
			                     <div class="clearfix"></div>
			                  		</div>
			                  <div class="col-md-6 col-sm-6">
			                     <!-- Small para -->
			                     <p class="ainfo">¿Eres un emprendedor? ¿Aventurero? ¿Te gusta la programación web? ¿Quieres unirte a este proyecto? Si eres decidido y te gustaría emprender una nueva aventura, no dudes en ponerte en contacto conmigo. </p>
			                     <input type='submit' class="button" value='Atrévete' name='atrevete'/>
			                  </div>
			               </div>
			            </div>
			         </div>
			   </div> 
				
				
			</div>
		</div>
		
		<!--End Content-->
	</div>
</div>

<!-- Start Footer -->
<div id="divFooter" class="footerArea">

    <div class="container">

        <div class="divPanel">

            <div class="row-fluid">
                <div class="col-lg-3 col-md-3">
                
                    <h3>Sobre la empresa</h3>

                    <p>Esta empresa fue creada desde el cariño y el esfuerzo de su autor durante el año 2014. Actualmente se encuentra en expansión.</p>
                    
                    <p> 
                        <a href="#" title="Terms of Use">Terms of Use</a><br />
                        <a href="#" title="Privacy Policy">Privacy Policy</a><br />
                        <a href="#" title="FAQ">FAQ</a><br />
                        <a href="#" title="Sitemap">Sitemap</a>
                    </p>

                </div>
                <div class="col-lg-6 col-md-6">

                    <h3>Sample Content</h3> 
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s. 
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s.
                    </p>
                    <p class="copyright">
                        Copyright © 2014 Paris Company. All Rights Reserved.
                    </p>

                </div>
                <div class="col-lg-3 col-md-3">

                    <h3>Mantente en contacto</h3>  
                                                               
                    <ul id="contact-info">
                    <li>                                    
                        <i class="general foundicon-phone icon"></i>
                        <span class="field">Phone:</span>
                        <br />
                        +34 675 67 79 71                                                                      
                    </li>
                    <li>
                        <i class="general foundicon-mail icon"></i>
                        <span class="field">Email:</span>
                        <br />
                        <a href="mailto:victorparis84@gmail.com" title="Email">victorparis84@gmail.com</a>
                    </li>
                    <li>
                        <i class="general foundicon-home icon" style="margin-bottom:50px"></i>
                        <span class="field">Address:</span>
                        <br />
                        10 Morrison Chambers, Nassau Street<br />
                        Dublin 2, Ireland<br />
                    </li>
                    </ul>

                </div>
            </div>

        </div>

    </div>
    
</div>

<!--End Container-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.js"></script>
<!-- All functions for this theme + document.ready processing -->
<script src="js/aerocontacts.js"></script>
</body>
</html>  


<?php
   }
?>















